#include <boost/test/unit_test.hpp>
#include "../MandelbrotCore/Precise.h"

namespace
{
   double random(double min, double max)
   {
      return rand() / (double) RAND_MAX * (max - min) + min;
   }
}

BOOST_AUTO_TEST_SUITE(TS_Precise)
BOOST_AUTO_TEST_SUITE(TS_Conversion)

BOOST_AUTO_TEST_CASE(IntConversion)
{
   // int constructor Precise
   {
      int v = 3;
      Precise pv(v);

      BOOST_CHECK_EQUAL(v, pv);
   }

   // int assigning to Precise
   {
      int v = 9;
      Precise pv;
      pv = v;

      BOOST_CHECK_EQUAL(v, pv);
   }

   // Precise to int
   {
      Precise pv = 5;
      int v = pv;

      BOOST_CHECK_EQUAL(pv, v);
   }
}

BOOST_AUTO_TEST_CASE(DoubleConversion)
{
   // double constructor Precise
   {
      double v = 3.32;
      Precise pv(v);

      BOOST_CHECK(pv.compare(10, v)); // compare only first 10 digits because double is less precise!
   }

   // double assigning to Precise
   {
      double v = 9.01200123;
      Precise pv;
      pv = v;

      BOOST_CHECK(pv.compare(10, v)); // compare only first 10 digits because double is less precise!
   }

   // Precise to double
   {
      Precise pv = 5.9201923;
      double v = pv;

      BOOST_CHECK(pv.compare(10, v)); // compare only first 10 digits because double is less precise!
   }
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE(TS_Logic)

BOOST_AUTO_TEST_CASE(IntLogic)
{
   {
      int v = 3;
      Precise pv(v + 1);

      BOOST_CHECK(pv > v);
      BOOST_CHECK(pv >= v);
      BOOST_CHECK(!(pv < v));
      BOOST_CHECK(!(pv <= v));
   }

   {
      int v = 3;
      Precise pv(v - 1);

      BOOST_CHECK(pv < v);
      BOOST_CHECK(pv <= v);
      BOOST_CHECK(!(pv > v));
      BOOST_CHECK(!(pv >= v));
   }
}

BOOST_AUTO_TEST_CASE(DoubleLogic)
{
   {
      double v = 3.1231;
      Precise pv(v + 1);

      BOOST_CHECK(pv > v);
      BOOST_CHECK(pv >= v);
      BOOST_CHECK(!(pv < v));
      BOOST_CHECK(!(pv <= v));
   }

   {
      double v = 3.1231;
      Precise pv(v - 1);

      BOOST_CHECK(pv < v);
      BOOST_CHECK(pv <= v);
      BOOST_CHECK(!(pv > v));
      BOOST_CHECK(!(pv >= v));
   }
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE(TS_Arithmetic)

BOOST_AUTO_TEST_CASE(IntAddition)
{
   {
      int v = 3;
      Precise pv(3);

      BOOST_CHECK_EQUAL(6, pv + v);
   }
   {
      int v = 3;
      Precise pv(3);

      pv += v;

      BOOST_CHECK_EQUAL(6, pv);
   }
   {
      auto pv = 5p;
      pv += 1;

      BOOST_CHECK_EQUAL(6, pv);
   }
   {
      int v = -3;
      Precise pv(3);

      BOOST_CHECK_EQUAL(0, pv + v);
   }
}

BOOST_AUTO_TEST_CASE(DoubleAddition)
{
   {
      double v = 3.123;
      auto pv = 3.123p;

      BOOST_CHECK((pv + v).compare(10, 6.246));// compare only first 10 digits because double is less precise!
   }
   {
      double v = -3.123;
      Precise pv(3);

      BOOST_CHECK((pv + v).compare(10, -0.123));// compare only first 10 digits because double is less precise!
   }
}

BOOST_AUTO_TEST_CASE(IntenseAdditionx100)
{
   srand(std::chrono::system_clock::now().time_since_epoch().count());

   for (int i = 0; i < 100; i++)
   {
      double a = random(-10, 10);
      double b = random(-10, 10);
      Precise pa = a;
      Precise pb = b;

      double r = a + b;
      Precise pr = pa + pb;

      auto result = pr.compare(10, r);
      BOOST_CHECK(result);// compare only first 10 digits because double is less precise!
      if (!result)
      {
         BOOST_TEST_MESSAGE(a << " + " << b << " = " << r);
         BOOST_TEST_MESSAGE(pa << " + " << pb << " = " << pr);
      }
   }
}

BOOST_AUTO_TEST_CASE(IntSubtraction)
{
   {
      int v = 3;
      Precise pv(3);

      BOOST_CHECK_EQUAL(0, pv - v);
   }
   {
      int v = 3;
      Precise pv(3);

      pv -= v;

      BOOST_CHECK_EQUAL(0, pv);
   }
   {
      auto pv = 5p;
      pv -= 1;

      BOOST_CHECK_EQUAL(4, pv);
   }
   {
      int v = -3;
      Precise pv(3);

      BOOST_CHECK_EQUAL(6, pv - v);
   }
}

BOOST_AUTO_TEST_CASE(DoubleSubtraction)
{
   {
      double v = 3.123;
      auto pv = 3.123p;

      BOOST_CHECK((pv - v).compare(10, 0));// compare only first 10 digits because double is less precise!
   }
   {
      double v = -3.123;
      Precise pv(3);

      BOOST_CHECK((pv - v).compare(10, 6.123));// compare only first 10 digits because double is less precise!
   }
}

BOOST_AUTO_TEST_CASE(IntenseSubtractionx100)
{
   srand(std::chrono::system_clock::now().time_since_epoch().count());

   for (int i = 0; i < 100; i++)
   {
      double a = random(-10, 10);
      double b = random(-10, 10);
      Precise pa = a;
      Precise pb = b;

      double r = a - b;
      Precise pr = pa - pb;

      auto result = pr.compare(10, r);
      BOOST_CHECK(result);// compare only first 10 digits because double is less precise!
      if (!result)
      {
         BOOST_TEST_MESSAGE(a << " - " << b << " = " << r);
         BOOST_TEST_MESSAGE(pa << " - " << pb << " = " << pr);
      }
   }
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE(TS_PreDefinedTypes)

BOOST_AUTO_TEST_CASE(SmallTypes)
{
   BOOST_TEST_MESSAGE(__milli);
   BOOST_TEST_MESSAGE(__micro);
   BOOST_TEST_MESSAGE(__nano);
   BOOST_TEST_MESSAGE(__pico);
   BOOST_TEST_MESSAGE(__femto);
   BOOST_TEST_MESSAGE(__atto);
   BOOST_TEST_MESSAGE(__zepto);
   BOOST_TEST_MESSAGE(__yocto);

   BOOST_TEST_MESSAGE("1E-100 = " << Precise(true, 100));

   BOOST_TEST_MESSAGE(
      __milli +
      __micro +
      __nano +
      __pico +
      __femto +
      __atto +
      __zepto +
      __yocto
   );

   BOOST_TEST_MESSAGE("1e-100 bytes: " << Precise(true, 100).getByteSize());
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()