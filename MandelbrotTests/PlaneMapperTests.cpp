#include <boost/test/unit_test.hpp>

#include "Conversion.h"
#include "../MandelbrotCore/PlaneMapper.h"
#include "../MandelbrotCore/PlaneDefinition.h"

namespace
{
   double random(double min, double max)
   {
      return rand() / (double) RAND_MAX * (max - min) + min;
   }

   Complex roundToDecimal(unsigned decimal, const Complex& point)
   {
      return Complex
      {
         std::round(point.real() * std::pow(10, decimal)) / std::pow(10, decimal),
         std::round(point.imag() * std::pow(10, decimal)) / std::pow(10, decimal)
      };
   }

   QPointF roundToDecimal(unsigned decimal, const QPointF& point)
   {
      return QPointF
      {
         (qreal) std::round(point.x() * std::pow(10, decimal)) / std::pow(10, decimal),
         (qreal) std::round(point.y() * std::pow(10, decimal)) / std::pow(10, decimal)
      };
   }
}

BOOST_AUTO_TEST_SUITE(TS_PlaneMapper)

BOOST_AUTO_TEST_CASE(PixelToComplex)
{
   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 1.0;

      Complex result = mapper.translateToComplex({0,0});
      Complex expected = {0,0};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 1.0;

      Complex result = mapper.translateToComplex({150, 150});
      Complex expected = {150, -150};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;

      Complex result = mapper.translateToComplex({150, 150});
      Complex expected = {75, -75};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;
      definition.ScaleX = 2.0;

      Complex result = mapper.translateToComplex({150, 150});
      Complex expected = {37.5, -75};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;
      definition.ScaleY = 2.0;

      Complex result = mapper.translateToComplex({150, 150});
      Complex expected = {75, 37.5};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;
      definition.ScaleY = 1 / 2.0;

      Complex result = mapper.translateToComplex({150, 150});
      Complex expected = {75, 150};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;
      definition.ScaleX = 1 / 2.0;

      Complex result = mapper.translateToComplex({150, 150});
      Complex expected = {150, -75};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;
      definition.ScaleX = 2.0;
      definition.ScaleY = 2.0;

      Complex result = mapper.translateToComplex({150, 150});
      Complex expected = {37.5, 37.5};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;
      definition.ScaleX = 2.0;
      definition.ScaleY = 2.0;
      definition.GlobalOffset = {0, 2};

      Complex result = mapper.translateToComplex({150, 150});
      Complex expected = {37.5, 35.5};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;
      definition.ScaleX = 2.0;
      definition.ScaleY = 2.0;
      definition.GlobalOffset = {2, -2};

      Complex result = mapper.translateToComplex({150, 150});
      Complex expected = {35.5, 39.5};

      BOOST_CHECK_EQUAL(expected, result);
   }
}

BOOST_AUTO_TEST_CASE(ComplexToPixel)
{
   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 1.0;

      QPointF result = mapper.translateToPixel({0,0});
      QPointF expected = {0,0};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 1.0;

      QPointF result = mapper.translateToPixel({150, 150});
      QPointF expected = {150, -150};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;

      QPointF result = mapper.translateToPixel({150, 150});
      QPointF expected = {300, -300};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;
      definition.ScaleX = 2.0;

      QPointF result = mapper.translateToPixel({150, 150});
      QPointF expected = {600, -300};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;
      definition.ScaleY = 2.0;

      QPointF result = mapper.translateToPixel({150, 150});
      QPointF expected = {300, 600};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;
      definition.ScaleY = 1 / 2.0;

      QPointF result = mapper.translateToPixel({150, 150});
      QPointF expected = {300, 150};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;
      definition.ScaleX = 1 / 2.0;

      QPointF result = mapper.translateToPixel({150, 150});
      QPointF expected = {150, -300};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;
      definition.ScaleX = 2.0;
      definition.ScaleY = 2.0;

      QPointF result = mapper.translateToPixel({150, 150});
      QPointF expected = {600, 600};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;
      definition.ScaleX = 2.0;
      definition.ScaleY = 2.0;
      definition.GlobalOffset = {0, 2};

      QPointF result = mapper.translateToPixel({150, 150});
      QPointF expected = {600, 608};

      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;
      definition.ScaleX = 2.0;
      definition.ScaleY = 2.0;
      definition.GlobalOffset = {2, -2};

      QPointF result = mapper.translateToPixel({150, 150});
      QPointF expected = {608, 592};

      BOOST_CHECK_EQUAL(expected, result);
   }
}

BOOST_AUTO_TEST_CASE(PixelToComplexOffset)
{
   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 1.0;
      definition.GlobalOffset = {1, 1};

      Complex result = mapper.translateToComplex({0, 0});
      Complex expected = {-1, -1};
      
      BOOST_CHECK_EQUAL(expected, result);
   }

   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = 2.0;
      definition.GlobalOffset = {1, 1};

      Complex result = mapper.translateToComplex({0, 0});
      Complex expected = {-1, -1};

      BOOST_CHECK_EQUAL(expected, result);
   }
}

BOOST_AUTO_TEST_CASE(ComplexToPixelToComplex_x100)
{
   srand(std::chrono::system_clock::now().time_since_epoch().count());

   for (int i = 0; i < 100; i++)
   {
      PlaneMapper mapper;
      PlaneDefinition& definition = mapper.getPlaneDefinition();
      definition.GlobalZoom = random(-5, 5);
      definition.GlobalOffset = {random(-3, 3), random(-3, 3)};

      Complex point = {random(-3, 3), random(-3, 3)};
      Complex result = mapper.translateToComplex(mapper.translateToPixel(point));

      Complex point_rounded = roundToDecimal(5, point);
      Complex result_rounded = roundToDecimal(5, result);

      BOOST_CHECK_EQUAL(result_rounded, point_rounded);
   }
}

BOOST_AUTO_TEST_CASE(TEST)
{
   PlaneMapper mapper;
   PlaneDefinition& def = mapper.getPlaneDefinition();
   def.GlobalOffset = {1, 1};
   def.GlobalZoom = 1;
   def.ViewSize = {10, 10};
   def.ScaleX = 1;
   def.ScaleY = 1;

   BOOST_CHECK_EQUAL(QPointF(6, 6), mapper.translateToPixel({0, 0}));
}

BOOST_AUTO_TEST_SUITE_END();