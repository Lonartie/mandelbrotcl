#include <boost/test/unit_test.hpp>
#include "../MandelbrotCore/ComplexMath.h"

BOOST_AUTO_TEST_SUITE(TS_ComplexMath)

BOOST_AUTO_TEST_CASE(Nearest10nthBase, *boost::unit_test::tolerance(1e-10L))
{
   BOOST_CHECK_EQUAL(0.1, nearest10nthBase(0.1232));
   BOOST_CHECK_EQUAL(10, nearest10nthBase(8.239));
   BOOST_CHECK_EQUAL(10, nearest10nthBase(49.1239));
   BOOST_CHECK_EQUAL(100, nearest10nthBase(52.1239));
   BOOST_CHECK_EQUAL(0.01, nearest10nthBase(0.049139));
   BOOST_CHECK_EQUAL(0.001, nearest10nthBase(0.00092139));
}

BOOST_AUTO_TEST_CASE(RoundToDecimalPrecision, *boost::unit_test::tolerance(1e-10L))
{
   BOOST_CHECK_EQUAL(10, roundToDecimalPrecision(-1, 11.123));
   BOOST_CHECK_EQUAL(500, roundToDecimalPrecision(-2, 542.123));
   BOOST_CHECK_EQUAL(1, roundToDecimalPrecision(0, 0.9123));
   BOOST_CHECK_EQUAL(0.003, roundToDecimalPrecision(3, 0.0026));
   BOOST_CHECK_EQUAL(0.9, roundToDecimalPrecision(1, 0.88888));
   BOOST_CHECK_EQUAL(0.00005, roundToDecimalPrecision(5, 0.000054293));
}

BOOST_AUTO_TEST_SUITE_END()