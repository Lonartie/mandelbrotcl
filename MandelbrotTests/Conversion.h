#pragma once

#include <iostream>
#include "../Mandelbrot/stdafx.h"

template<typename T>
inline std::ostream& operator<<(std::ostream& stream, const std::complex<T> & compl)
{
   return stream << compl.real() << (compl.imag() < 0 ? "" : "+") << compl.imag() << "i";
}

inline std::ostream& operator<<(std::ostream& stream, const QPointF & point)
{
   return stream << point.x() << "/" << point.y();
}

inline std::ostream& operator<<(std::ostream& stream, const QPoint& point)
{
   return stream << point.x() << "/" << point.y();
}
