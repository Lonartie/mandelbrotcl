#pragma once

#include "stdafx.h"
#include "MainView.h"
#include "FractalSetPresenter.h"

#include "../MandelbrotCore/MandelbrotSetGenerator.h"
#include "../MandelbrotCore/JuliaSetGenerator.h"

class MainPresenter : public QObject
{
   Q_OBJECT

public /*constructors*/:

    MainPresenter(MainView& view, QObject *parent = Q_NULLPTR);
    ~MainPresenter() = default;

private slots:

   void setJuliaOffset(const QPointF& pos, Qt::MouseButton button);

private /*members*/:

   MainView& m_view;
   
   MandelbrotSetGenerator m_mandelbrotSetGenerator;
   JuliaSetGenerator m_juliaSetGenerator;

   FractalSetPresenter m_mandelbrotPresenter;
   FractalSetPresenter m_juliaPresenter;
};
