#pragma once

#include "stdafx.h"
#include "FractalSetView.h"
#include "FractalSetModel.h"
#include "AxisDrawer.h"

#include "../MandelbrotCore/FractalSetGenerator.h"

class FractalSetPresenter : public QObject
{
   Q_OBJECT

public /*constructors*/:

    FractalSetPresenter(FractalSetView& view, FractalSetGenerator& generator, QObject *parent = Q_NULLPTR);
    ~FractalSetPresenter() = default;
   
signals:

   void imageReady(const QImage& image);

public slots:

   void updateView();
   void updateXAxis();
   void updateYAxis();

private slots:

   void keyPressed(Qt::Key key);
   void animationTick();

private /*members*/:

   FractalSetView& m_view;
   FractalSetGenerator& m_generator;
   FractalSetModel m_model;

   AxisDrawer m_xDrawer;
   AxisDrawer m_yDrawer;

   QTimer m_animationTimer;
   bool m_animationRunning;
};
