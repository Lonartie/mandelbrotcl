#include "stdafx.h"
#include "FractalSetView.h"

namespace
{
   QScreen* getActiveScreen(QWidget* pWidget)
   {
      QScreen* pActive = nullptr;

      while (pWidget)
      {
         auto w = pWidget->windowHandle();
         if (w != nullptr)
         {
            pActive = w->screen();
            break;
         } else
            pWidget = pWidget->parentWidget();
      }

      return pActive;
   }
}

FractalSetView::FractalSetView(QWidget* parent /*= Q_NULLPTR*/)
   : QWidget(parent)
{
   ui.setupUi(this);
   m_infoMenu.setWindowFlags(Qt::FramelessWindowHint);

   connect(ui.info, &QPushButton::pressed, this, &FractalSetView::showInfoMenu);
   connect(ui.view, &MultiLabel::resized, this, [this](auto& oldSize, auto& newSize) { emit viewResized(newSize); });
   m_infoMenu.setColumns(5);

   for (const auto& mapping : ColorMapping::registered)
      m_infoMenu.addColorMapping(mapping);
}

MultiLabel& FractalSetView::getAxis(AxisDrawer::AxisType axis)
{
   switch (axis)
   {
   case AxisDrawer::X_Axis:
      return *ui.x_axis;
   case AxisDrawer::Y_Axis:
      return *ui.y_axis;
   default:
      return *ui.x_axis;
   }
}

PlaneDrawer& FractalSetView::getPlane()
{
   return *ui.view;
}

InfoMenu& FractalSetView::getInfoMenu()
{
   return m_infoMenu;
}

void FractalSetView::setDataTitle(const QString& title)
{
   ui.dataView->setTitle(title);
}

void FractalSetView::keyPressEvent(QKeyEvent* event)
{
   emit keyPressed(static_cast<Qt::Key>(event->key()));
   QWidget::keyPressEvent(event);
}

void FractalSetView::showInfoMenu()
{
   auto pos = QCursor::pos();
   auto geo = getActiveScreen(this)->geometry();

   // clamp info widget to screen bounds of active screen
   if (pos.x() + m_infoMenu.width() > geo.width())
      pos.setX(geo.width() - m_infoMenu.width());
   if (pos.y() + m_infoMenu.height() > geo.height())
      pos.setY(geo.height() - m_infoMenu.height());

   m_infoMenu.move(pos);
   m_infoMenu.show();
   m_infoMenu.setFocus(Qt::PopupFocusReason);
}
