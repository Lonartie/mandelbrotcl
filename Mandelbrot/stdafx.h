#pragma once

#include <QtCore>
#include <QtGUI>
#include <QtWidgets>
#include <QtConcurrent/QtConcurrent>

#include <memory>
#include <vector>
#include <string>
#include <map>
#include <functional>
#include <thread>
#include <cassert>
#include <math.h>
#include <algorithm>
#include <xutility>
#include <array>
#include <cmath>
#include <complex>

using Complex = std::complex<long double>;
