#include "stdafx.h"
#include "FractalSetPresenter.h"
#include "MultiLabel.h"

#define _CAT(a, b) a##b
#define CAT(a, b) _CAT(a, b)
#define _ auto CAT(_t, __LINE__) = 

FractalSetPresenter::FractalSetPresenter(FractalSetView& view, FractalSetGenerator& generator, QObject* parent /*= Q_NULLPTR*/)
   : QObject(parent)
   , m_view(view)
   , m_generator(generator)
   , m_model()
   , m_xDrawer(m_view.getAxis(AxisDrawer::X_Axis), AxisDrawer::X_Axis)
   , m_yDrawer(m_view.getAxis(AxisDrawer::Y_Axis), AxisDrawer::Y_Axis)
   , m_animationTimer()
   , m_animationRunning(false)
{
   _ connect(&m_generator, &FractalSetGenerator::imageReady, &m_view.getPlane(), qOverload<QImage*>(&PlaneDrawer::setImage));
   _ connect(&m_view.getPlane(), &PlaneDrawer::becameDirty, this, &FractalSetPresenter::updateView);
   _ connect(&m_view.getPlane(), &PlaneDrawer::paintBegin, this, &FractalSetPresenter::updateXAxis);
   _ connect(&m_view.getPlane(), &PlaneDrawer::paintBegin, this, &FractalSetPresenter::updateYAxis);
   _ connect(&m_view.getPlane(), &PlaneDrawer::verticalLinesDrawn, &m_xDrawer, &AxisDrawer::drawVerticalTicks);
   _ connect(&m_view.getPlane(), &PlaneDrawer::horizontalLinesDrawn, &m_yDrawer, &AxisDrawer::drawHorizontalTicks);
   _ connect(&m_view.getPlane(), &PlaneDrawer::paintDone, &m_xDrawer, &AxisDrawer::pushImage);
   _ connect(&m_view.getPlane(), &PlaneDrawer::paintDone, &m_yDrawer, &AxisDrawer::pushImage);
   _ connect(&m_view, &FractalSetView::keyPressed, this, &FractalSetPresenter::keyPressed);
   _ connect(&m_animationTimer, &QTimer::timeout, this, &FractalSetPresenter::animationTick);
   _ connect(&m_view.getInfoMenu(), &InfoMenu::colorMappingSelected, &m_generator, &FractalSetGenerator::setColorMapper);
   _ connect(&m_view.getInfoMenu(), &InfoMenu::colorMappingSelected, this, &FractalSetPresenter::updateView);
}

void FractalSetPresenter::updateView()
{
   auto& mapper = m_view.getPlane().getPlaneMapper();
   m_generator.generateSet(mapper);
}

void FractalSetPresenter::updateXAxis()
{
   m_xDrawer.drawAxis(m_model.Plane.getPlaneDefinition());
}

void FractalSetPresenter::updateYAxis()
{
   m_yDrawer.drawAxis(m_model.Plane.getPlaneDefinition());
}

void FractalSetPresenter::keyPressed(Qt::Key key)
{
   if (key == Qt::Key::Key_Return)
   {
      if (m_animationRunning)
         m_animationTimer.stop();
      else
         m_animationTimer.start(std::chrono::milliseconds(16));

      m_animationRunning = !m_animationRunning;
   }
}

void FractalSetPresenter::animationTick()
{
   m_view.getPlane().getPlaneMapper().getPlaneDefinition().GlobalZoom *= 1.01;
   updateView();
}