#include "stdafx.h"
#include "InfoMenu.h"

InfoMenu::InfoMenu(QWidget *parent)
    : QWidget(parent)
{
    ui.setupUi(this);
    setFocusPolicy(Qt::FocusPolicy::StrongFocus);
    adjustSize();
}

void InfoMenu::setColumns(unsigned colCount)
{
   m_columns = colCount;
}

void InfoMenu::addColorMapping(const ColorMapping& mapping)
{
   m_mappingMap.emplace(std::pair<int, int>(m_currentCol, m_currentRow), mapping);
   createButton(mapping);

   m_currentCol++;
   if (m_currentCol + 1 >= m_columns)
   {
      m_currentCol = 0;
      m_currentRow++;
   }
}

void InfoMenu::focusOutEvent(QFocusEvent* event)
{
   hide();
}

void InfoMenu::createButton(const ColorMapping& mapping)
{
   QPushButton* button = new QPushButton();
   button->setFlat(true);
   QImage image(mapping.toImage().scaled(10, 50));
   button->setFixedSize(image.size());
   button->setIcon(QPixmap::fromImage(image));
   button->setIconSize(image.size());
   auto layout = qobject_cast<QGridLayout*>(ui.content->layout());
   layout->addWidget(button, m_currentRow, m_currentCol, 1, 1, Qt::AlignCenter);
   adjustSize();

   connect(button, &QPushButton::clicked, this, [this, mapping]()
   {
      emit colorMappingSelected(mapping);
   });
}
