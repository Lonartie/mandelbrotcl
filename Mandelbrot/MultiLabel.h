#pragma once

#include "stdafx.h"

class MultiLabel : public QLabel
{
   Q_OBJECT

public /*constructors*/:

    MultiLabel(QWidget* parent = Q_NULLPTR);
    virtual ~MultiLabel() = default;

signals:

   void mouseDown(const QPointF& pos, Qt::MouseButton button);
   void mouseUp(const QPointF& pos, Qt::MouseButton button);
   void mouseEntered(const QPointF& pos);
   void mouseMoved(const QPointF& pos);
   void mouseLeaved(const QPointF& pos);
   void shown();
   void hidden();
   void resized(const QSizeF& oldSize, const QSizeF& newSize);
   void scrolled(const QPointF& angleDelta);

public slots:

   void setImage(QImage&& image);
   void setImage(const QImage& image);

protected /*overriding: QLabel->QFrame->QWidget*/:

   virtual void enterEvent(QEvent* event) override;
   virtual void mouseMoveEvent(QMouseEvent* event) override;
   virtual void leaveEvent(QEvent* event) override;
   virtual void mousePressEvent(QMouseEvent* event) override;
   virtual void mouseReleaseEvent(QMouseEvent* event) override;
   virtual void resizeEvent(QResizeEvent* event) override;
   virtual void showEvent(QShowEvent* event) override;
   virtual void hideEvent(QHideEvent* event) override;
   virtual void wheelEvent(QWheelEvent* event) override;

private /*members*/:

   int fixedW = -1, fixedH = -1;

};
