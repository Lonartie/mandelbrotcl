#pragma once

#include "stdafx.h"
#include "MultiLabel.h"
#include "../MandelbrotCore/PlaneDefinition.h"

class AxisDrawer : public QObject
{
   Q_OBJECT

public /*constructor*/:

   enum AxisType { X_Axis, Y_Axis };
   
   AxisDrawer(MultiLabel& view, AxisType axisType, QObject* parent = Q_NULLPTR);
   virtual ~AxisDrawer() = default;
   
public slots:

   void drawAxis(const PlaneDefinition& def);
   void drawVerticalTicks(std::vector<std::pair<unsigned /*pixel*/, long double /*value*/>> ticks);
   void drawHorizontalTicks(std::vector<std::pair<unsigned /*pixel*/, long double /*value*/>> ticks);
   void pushImage();
   
private /*functions*/:

   static inline void drawVerticalLine(QPainter& painter, const QSizeF& imageSize);
   static inline void drawHorizontalLine(QPainter& painter, const QSizeF& imageSize);
   static inline void drawVerticalUnit(QPainter& painter, const QSizeF& imageSize);
   static inline void drawHorizontalUnit(QPainter& painter, const QSizeF& imageSize);

private /*members*/:

   MultiLabel& m_view;
   AxisType m_axisType;
   QImage m_image;
   double m_fontSizeMultiplier = .75;
};

