#include "stdafx.h"
#include "MainPresenter.h"

MainPresenter::MainPresenter(MainView& view, QObject* parent /*= Q_NULLPTR*/)
   : QObject(parent)
   , m_view(view)
   , m_mandelbrotSetGenerator()
   , m_juliaSetGenerator()
   , m_mandelbrotPresenter(m_view.getMandelbrotView(), m_mandelbrotSetGenerator, this)
   , m_juliaPresenter(m_view.getJuliaView(), m_juliaSetGenerator, this)
{
   m_view.getMandelbrotView().setDataTitle("Mandelbrot set");
   m_view.getJuliaView().setDataTitle("Julia set");

   connect(&m_view.getMandelbrotView().getPlane(), &PlaneDrawer::mouseDown, this, &MainPresenter::setJuliaOffset);
   connect(&m_view.getMandelbrotView().getPlane(), &PlaneDrawer::mouseDown, &m_juliaPresenter, &FractalSetPresenter::updateView);
}

void MainPresenter::setJuliaOffset(const QPointF& pos, Qt::MouseButton button)
{
   if (button == Qt::MouseButton::LeftButton)
      m_juliaSetGenerator.setOffsetPosition(m_view.getMandelbrotView().getPlane().getPlaneMapper().translateToComplex(pos));
}

