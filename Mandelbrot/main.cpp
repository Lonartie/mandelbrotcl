#include "stdafx.h"
#include "MainView.h"
#include "MainPresenter.h"

int main(int argc, char* argv[])
{
   QApplication a(argc, argv);
   MainView view;
   MainPresenter presenter(view);
   view.show();
   return a.exec();
}
