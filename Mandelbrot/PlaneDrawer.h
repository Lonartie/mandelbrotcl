#pragma once

#include "stdafx.h"
#include "MultiLabel.h"
#include "../MandelbrotCore/PlaneMapper.h"

class PlaneDrawer: public MultiLabel
{
   Q_OBJECT

public /*constructor*/:

   PlaneDrawer(QWidget* parent = Q_NULLPTR);
   ~PlaneDrawer() = default;

public /*functions*/:

   void setPlaneMapper(const PlaneMapper& mapper);
   const PlaneMapper& getPlaneMapper() const;
   PlaneMapper& getPlaneMapper();
   void update(QImage&& image);

signals:

   void paintBegin();
   void verticalLinesDrawn(std::vector<std::pair<unsigned /*pixel*/, long double /*value*/>> ticks);
   void horizontalLinesDrawn(std::vector<std::pair<unsigned /*pixel*/, long double /*value*/>> ticks);
   void paintDone();
   void becameDirty();

public slots:

   void showGrid(bool show);
   virtual void setImage(QImage& image);
   virtual void setImage(QImage* image);
   void zoom(const QPointF& angleDelta);
   void zoomAxis(const QPointF& angleDelta, PlaneMapper::Axis axis);

private slots:

   void resize(const QSizeF& oldSize, const QSizeF& newSize);
   void beginMoveView(const QPointF& pos, Qt::MouseButton button);
   void moveView(const QPointF& pos);
   void endMoveView(const QPointF& pos, Qt::MouseButton button);

private /*functions*/:

   QImage&& drawGrid(QImage&& image);
   void processViewMove(const QPointF& moveDelta);

private /*members*/:

   PlaneMapper m_mapper;
   bool m_showGrid;
   QImage m_imageWithGrid;
   QImage m_imageNoGrid;
   bool m_viewMoving;
   QPointF m_lastMovePosition;
   Qt::MouseButton m_moveMouseButton;
};
