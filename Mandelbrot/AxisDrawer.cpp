#include "stdafx.h"
#include "AxisDrawer.h"

AxisDrawer::AxisDrawer(MultiLabel& view, AxisType axisType, QObject* parent /*= Q_NULLPTR*/)
   : QObject(parent)
   , m_view(view)
   , m_axisType(axisType)
{
}

void AxisDrawer::drawAxis(const PlaneDefinition& def)
{
   // TODO check if delayed is necessary
   QImage image(m_view.size(), QImage::Format_ARGB32);
   image.fill(Qt::transparent);
   QPainter painter(&image);

   switch (m_axisType)
   {
   case X_Axis:
   {
      drawHorizontalLine(painter, m_view.size());
      drawHorizontalUnit(painter, m_view.size());
      break;
   }
   case Y_Axis:
   {
      drawVerticalLine(painter, m_view.size());
      drawVerticalUnit(painter, m_view.size());
      break;
   }
   }

   m_image = image;
}



void AxisDrawer::drawVerticalLine(QPainter& painter, const QSizeF& imageSize)
{
   QPointF start_pos = {imageSize.width() - 1, 1};
   QPointF end_pos = {imageSize.width() - 1, imageSize.height() - 1};
   QBrush brush(Qt::black);
   painter.setBrush(brush);
   painter.drawLine(start_pos, end_pos);
}

void AxisDrawer::drawHorizontalLine(QPainter& painter, const QSizeF& imageSize)
{
   QPointF start_pos = {1, 1};
   QPointF end_pos = {imageSize.width() - 1, 1};
   QBrush brush(Qt::black);
   painter.setBrush(brush);
   painter.drawLine(start_pos, end_pos);
}

void AxisDrawer::drawVerticalUnit(QPainter& painter, const QSizeF& imageSize)
{
   QPointF top_left = {1, 1};
   QPointF bottom_right = {imageSize.width() - 3, 50};
   QBrush brush(Qt::black);
   painter.setBrush(brush);
   QTextOption to;
   to.setAlignment(Qt::AlignTop);
   painter.drawText(QRectF(top_left, bottom_right), "Y [i]", to);
}

void AxisDrawer::drawHorizontalUnit(QPainter& painter, const QSizeF& imageSize)
{
   QPointF top_left = {imageSize.width() - 50, 1};
   QPointF bottom_right = {imageSize.width() - 5, 25};
   QBrush brush(Qt::black);
   painter.setBrush(brush);
   QTextOption to;
   to.setAlignment(Qt::AlignRight);
   painter.drawText(QRectF(top_left, bottom_right), "X [R]", to);
}

void AxisDrawer::drawVerticalTicks(std::vector<std::pair<unsigned /*pixel*/, long double /*value*/>> ticks)
{
   QPainter painter(&m_image);
   QFont font = painter.font();
   font.setPointSize(font.pointSize() * m_fontSizeMultiplier);
   painter.setFont(font);
   QTextOption option;
   option.setAlignment(Qt::AlignCenter);
   painter.save();
   painter.translate(m_image.width() / 2, m_image.height() / 2);
   painter.rotate(-90);
   for (const auto& tick : ticks)
   {
      if (tick.first < m_image.width() - 25)
         painter.drawText(QRect(-m_image.height() / 2, tick.first - 10 - m_image.width() / 2, 30, 20), QString::number((double) tick.second, 'e', 0), option);
         //painter.drawText(QRect(tick.first - 15, 5, 30, 20), QString::number((double) tick.second, 'g', 1), option);
   }
   painter.rotate(90);
   painter.restore();
}

void AxisDrawer::drawHorizontalTicks(std::vector<std::pair<unsigned /*pixel*/, long double /*value*/>> ticks)
{
   QPainter painter(&m_image);
   QFont font = painter.font();
   font.setPointSize(font.pointSize() * m_fontSizeMultiplier);
   painter.setFont(font);
   QTextOption option;
   option.setAlignment(Qt::AlignCenter);
   for (const auto& tick : ticks)
   {
      if (tick.first > 25)
      painter.drawText(QRect(0, tick.first - 10, 30, 20), QString::number((double)tick.second, 'e', 0), option);
   }
}

void AxisDrawer::pushImage()
{
   m_view.setImage(m_image);
}
