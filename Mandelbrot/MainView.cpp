#include "stdafx.h"
#include "MainView.h"

MainView::MainView(QWidget* parent)
   : QMainWindow(parent)
{
   ui.setupUi(this);
}

FractalSetView& MainView::getMandelbrotView() const
{
   return *ui.mandelbrot_view;
}

FractalSetView& MainView::getJuliaView() const
{
   return *ui.julia_view;
}

void MainView::resizeEvent(QResizeEvent* event)
{
   emit resized(event->size());
   QMainWindow::resizeEvent(event);
}
