#include "stdafx.h"
#include "MultiLabel.h"

MultiLabel::MultiLabel(QWidget* parent /*= Q_NULLPTR*/)
   : QLabel(parent)
{}

void MultiLabel::setImage(QImage&& image)
{
   const QImage tmp(std::move(image));
   setImage(tmp);
}

void MultiLabel::setImage(const QImage& image)
{
   setPixmap(QPixmap::fromImage(image));

   if (sizePolicy().horizontalPolicy() == QSizePolicy::Fixed && sizePolicy().verticalPolicy() != QSizePolicy::Fixed)
   {
      QSizePolicy policy(QSizePolicy::Fixed, QSizePolicy::MinimumExpanding);
      policy.setHorizontalStretch(1);
      policy.setVerticalStretch(1);
      setSizePolicy(policy);
      setMinimumSize(QSize(minimumSize().width(), 1));
   } else if (sizePolicy().horizontalPolicy() != QSizePolicy::Fixed && sizePolicy().verticalPolicy() == QSizePolicy::Fixed)
   {
      QSizePolicy policy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
      policy.setHorizontalStretch(1);
      policy.setVerticalStretch(1);
      setSizePolicy(policy);
      setMinimumSize(QSize(1, minimumSize().height()));
   } else if (sizePolicy().horizontalPolicy() == QSizePolicy::Fixed && sizePolicy().verticalPolicy() == QSizePolicy::Fixed)
   {
      QSizePolicy policy(QSizePolicy::Fixed, QSizePolicy::Fixed);
      policy.setHorizontalStretch(1);
      policy.setVerticalStretch(1);
      setSizePolicy(policy);
      setMinimumSize(minimumSize());
   } else
   {
      QSizePolicy policy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
      policy.setHorizontalStretch(1);
      policy.setVerticalStretch(1);
      setSizePolicy(policy);
      setMinimumSize(QSize(1, 1));
   }
}

void MultiLabel::enterEvent(QEvent* event)
{
   emit mouseEntered(mapFromGlobal(QCursor::pos()));

   QLabel::enterEvent(event);
}

void MultiLabel::mouseMoveEvent(QMouseEvent* event)
{
   emit mouseMoved(event->pos());

   QLabel::mouseMoveEvent(event);
}

void MultiLabel::leaveEvent(QEvent* event)
{
   emit mouseLeaved(mapFromGlobal(QCursor::pos()));

   QLabel::leaveEvent(event);
}

void MultiLabel::mousePressEvent(QMouseEvent* event)
{
   emit mouseDown(event->pos(), event->button());

   QLabel::mousePressEvent(event);
}

void MultiLabel::mouseReleaseEvent(QMouseEvent* event)
{
   emit mouseUp(event->pos(), event->button());

   QLabel::mouseReleaseEvent(event);
}

void MultiLabel::resizeEvent(QResizeEvent* event)
{
   emit resized(event->oldSize(), event->size());

   QLabel::resizeEvent(event);
}

void MultiLabel::showEvent(QShowEvent* event)
{
   emit show();

   QLabel::showEvent(event);
}

void MultiLabel::hideEvent(QHideEvent* event)
{
   emit hidden();

   QLabel::hideEvent(event);
}

void MultiLabel::wheelEvent(QWheelEvent* event)
{
   emit scrolled(event->angleDelta());

   QLabel::wheelEvent(event);
}
