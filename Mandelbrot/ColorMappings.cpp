#include "stdafx.h"
#include "../MandelbrotCore/ColorMapping.h"

#define _CAT(a, b) a##b
#define CAT(a, b) _CAT(a, b)
#define CreateMapping(var, r, g, b) static auto CAT(CAT(Mapping, __LINE__), Registered) = ColorMapping::registerMapping(ColorMapping([](auto var) r, [](auto var) g, [](auto var) b));


// standard
CreateMapping(
   value,
   {return value * 255;},
   {return value * 255;},
   {return value * 255;}
);

// reversed - standard
CreateMapping(
   value,
   {return 255 - value * 255;},
   {return 255 - value * 255;},
   {return 255 - value * 255;}
);

// #1
CreateMapping(
   value,
   {return 255 - std::abs(std::cos(value)) * 255;},
   {return 255 - std::abs(std::sin(value)) * 255;},
   {return 255 - std::abs(std::cos(value)) * 255;}
);