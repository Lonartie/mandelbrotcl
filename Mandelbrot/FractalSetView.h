#pragma once

#include "stdafx.h"
#include "AxisDrawer.h"
#include "InfoMenu.h"
#include "ui_FractalSetView.h"

class FractalSetView : public QWidget
{
    Q_OBJECT

public /*constructor*/:

    FractalSetView(QWidget *parent = Q_NULLPTR);
    ~FractalSetView() = default;

    MultiLabel& getAxis(AxisDrawer::AxisType axis);
    PlaneDrawer& getPlane();
    InfoMenu& getInfoMenu();
    
signals:

   void viewResized(const QSizeF& size);
   void keyPressed(Qt::Key key);

public slots:
   void setDataTitle(const QString& title);

protected:

   virtual void keyPressEvent(QKeyEvent* event) override;

private slots:

   void showInfoMenu();

private /*members*/:

    Ui::FractalSetView ui;
    InfoMenu m_infoMenu;
};
