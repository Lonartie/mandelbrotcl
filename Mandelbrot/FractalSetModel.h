#pragma once

#include "stdafx.h"
#include "../MandelbrotCore/ComplexPlane.h"

struct FractalSetModel
{
   ComplexPlane Plane;
   Complex StartPosition = {0,0};
   Complex Offset = {0,0};
};