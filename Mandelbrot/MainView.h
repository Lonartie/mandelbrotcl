#pragma once

#include "stdafx.h"
#include "ui_MainView.h"

class MainView: public QMainWindow
{
   Q_OBJECT

public /*constructors*/:

   MainView(QWidget* parent = Q_NULLPTR);

public /*functions*/:

   FractalSetView& getMandelbrotView() const;
   FractalSetView& getJuliaView() const;

signals:

   void resized(const QSizeF& size);

protected:

   virtual void resizeEvent(QResizeEvent* event) override;

private /*members*/:

   Ui::MainViewClass ui;
};
