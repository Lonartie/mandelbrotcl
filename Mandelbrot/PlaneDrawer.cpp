#include "stdafx.h"
#include "PlaneDrawer.h"

#include "../MandelbrotCore/ComplexMath.h"

namespace
{
   Complex floor(const Complex& cmp)
   {
      return Complex{std::floor(cmp.real()), std::floor(cmp.imag())};
   }

   bool isPointInSize(const QSizeF& size, const QPointF& point)
   {
      if (point.x() < 0 || point.y() < 0) return false;
      if (point.x() > size.width() || point.y() > size.height()) return false;
      return true;
   }
}

PlaneDrawer::PlaneDrawer(QWidget* parent /*= Q_NULLPTR*/)
   : MultiLabel(parent)
   , m_mapper()
   , m_showGrid(true)
   , m_imageWithGrid()
   , m_imageNoGrid()
   , m_viewMoving(false)
   , m_lastMovePosition({0,0})
   , m_moveMouseButton(Qt::MouseButton::RightButton)
{
   connect(this, &MultiLabel::resized, this, &PlaneDrawer::resize);
   connect(this, &MultiLabel::scrolled, this, &PlaneDrawer::zoom);
   connect(this, &MultiLabel::mouseDown, this, &PlaneDrawer::beginMoveView);
   connect(this, &MultiLabel::mouseMoved, this, &PlaneDrawer::moveView);
   connect(this, &MultiLabel::mouseUp, this, &PlaneDrawer::endMoveView);
}

void PlaneDrawer::setPlaneMapper(const PlaneMapper& mapper)
{
   m_mapper = mapper;
}

const PlaneMapper& PlaneDrawer::getPlaneMapper() const
{
   return m_mapper;
}

PlaneMapper& PlaneDrawer::getPlaneMapper()
{
   return m_mapper;
}

void PlaneDrawer::showGrid(bool show)
{
   m_showGrid = show;
}

void PlaneDrawer::update(QImage&& image)
{
   emit paintBegin();

   if (m_showGrid)
   {
      MultiLabel::setImage(drawGrid(std::move(image)));
   } else
   {
      MultiLabel::setImage(std::move(image));
   }

   emit paintDone();
}

void PlaneDrawer::setImage(QImage& image)
{
   update(std::move(image));
}

void PlaneDrawer::setImage(QImage* image)
{
   update(std::move(*image));
}

void PlaneDrawer::resize(const QSizeF& oldSize, const QSizeF& newSize)
{
   m_mapper.getPlaneDefinition().ViewSize = newSize;

   emit becameDirty();
}

void PlaneDrawer::beginMoveView(const QPointF& pos, Qt::MouseButton button)
{
   if (button != m_moveMouseButton) return;

   m_viewMoving = true;
   m_lastMovePosition = pos;
}

void PlaneDrawer::moveView(const QPointF& pos)
{
   if (!m_viewMoving) return;

   auto moveDelta = (pos - m_lastMovePosition);
   m_lastMovePosition = pos;
   processViewMove(moveDelta);
}

void PlaneDrawer::endMoveView(const QPointF& pos, Qt::MouseButton button)
{
   if (button != m_moveMouseButton) return;

   m_viewMoving = false;
}

void PlaneDrawer::zoom(const QPointF& angleDelta)
{
   double& zoom = m_mapper.getPlaneDefinition().GlobalZoom;
   zoom *= angleDelta.y() > 0 ? 2 : 0.5;
   zoom = std::max(zoom, 1.0);

   emit becameDirty();
}

void PlaneDrawer::zoomAxis(const QPointF& angleDelta, PlaneMapper::Axis axis)
{
   double& zoom = axis == PlaneMapper::X ?
      m_mapper.getPlaneDefinition().ScaleX :
      m_mapper.getPlaneDefinition().ScaleY;

   zoom *= angleDelta.y() > 0 ? 2 : 0.5;
   zoom = std::max(zoom, 1.0);

   emit becameDirty();
}

QImage&& PlaneDrawer::drawGrid(QImage&& image)
{
   constexpr unsigned MaxDrawnLinesPerSide = 10;

   QSizeF thisSize = size();
   auto height = thisSize.height();
   auto width = thisSize.width();

   QPainter painter(&image);
   auto bounds = m_mapper.getBounds(thisSize);
   auto complex_start = bounds[0];
   auto complex_end = bounds[3];

   auto fmt_x = nearest10nthBase(0.5L * std::abs(complex_end.real() - complex_start.real()));
   auto fmt_y = nearest10nthBase(0.5L * std::abs(complex_end.imag() - complex_start.imag()));

   auto fmt = std::min(fmt_x, fmt_y);

   auto csr = roundToDecimalPrecision(-std::log10(fmt), complex_start);
   auto cer = roundToDecimalPrecision(-std::log10(fmt), complex_end);

   auto pixel_start = m_mapper.translateToPixel(csr);
   auto pixel_end = m_mapper.translateToPixel(cer);

   std::vector<std::pair<unsigned, long double>> vertical_list, horizontal_list;

   QPointF steps = QPointF
   {
      (qreal) std::abs(fmt * m_mapper.pixelPerComplex(PlaneMapper::X)),
      (qreal) std::abs(fmt * m_mapper.pixelPerComplex(PlaneMapper::Y)),
   };

   // draw vertical
   for (double x = pixel_start.x(); x <= pixel_end.x(); x += steps.x())
   {
      if (x > 0 && x < width)
      {
         painter.drawLine(x, 0, x, height - 1);
         vertical_list.push_back({x, m_mapper.translateToComplex(x, PlaneMapper::X)});
      }
   }

   emit verticalLinesDrawn(vertical_list);

   // draw horizontal
   for (double y = pixel_start.y(); y <= pixel_end.y(); y += steps.y())
   {
      if (y > 0 && y < height)
      {
         painter.drawLine(0, y, width - 1, y);
         horizontal_list.push_back({y, m_mapper.translateToComplex(y, PlaneMapper::Y)});
      }
   }

   emit horizontalLinesDrawn(horizontal_list);

   // draw origin red dot
   auto middle = m_mapper.translateToPixel({0,0});
   painter.setBrush(Qt::red);
   painter.drawEllipse(middle, 3, 3);

   // draw green middle dot
   QPoint middled(width / 2, height / 2);
   painter.setBrush(Qt::green);
   painter.drawEllipse(middled, 1, 1);

   return std::move(image);
}

void PlaneDrawer::processViewMove(const QPointF& moveDelta)
{
   auto& offset = m_mapper.getPlaneDefinition().GlobalOffset;
   auto x_factor = m_mapper.complexPerPixel(PlaneMapper::X);
   auto y_factor = m_mapper.complexPerPixel(PlaneMapper::Y);
   Complex additional_offset = {x_factor * moveDelta.x(), y_factor * moveDelta.y()};
   offset += additional_offset;

   emit becameDirty();
}
