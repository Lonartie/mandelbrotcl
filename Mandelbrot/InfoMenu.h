#pragma once

#include "stdafx.h"
#include "../MandelbrotCore/ColorMapping.h"
#include "ui_InfoMenu.h"

class InfoMenu : public QWidget
{
    Q_OBJECT

public:
    InfoMenu(QWidget *parent = Q_NULLPTR);
    ~InfoMenu() = default;

signals:

   void colorMappingSelected(const ColorMapping& colorMapping);

public slots:

   void setColumns(unsigned colCount);
   void addColorMapping(const ColorMapping& mapping);

protected:

   virtual void focusOutEvent(QFocusEvent* event) override;

private /*functions*/:

   void createButton(const ColorMapping& mapping);

private /*members*/:
    Ui::InfoMenu ui;
    unsigned m_columns = 0;
    unsigned m_currentCol = 0;
    unsigned m_currentRow = 0;
    std::map<std::pair<int, int>, ColorMapping> m_mappingMap;
};
