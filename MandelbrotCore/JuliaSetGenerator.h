#pragma once

#include "stdafx.h"
#include "FractalSetGenerator.h"

class MANDELBROTCORE_EXPORT JuliaSetGenerator : public FractalSetGenerator
{
   Q_OBJECT

public /*constructors*/:

   JuliaSetGenerator(QObject* parent = Q_NULLPTR);
   virtual ~JuliaSetGenerator() = default;

public slots:

   virtual void generateSet(const PlaneMapper& mapper) override;
   void setOffsetPosition(const Complex& offset);

private /*functions*/:

   double juliaIterations(const Complex& point);
   void generateSetInThread(const PlaneMapper& mapper);

private /*members*/:

   unsigned m_iterations;
   Complex m_startPos;
   QFuture<void> m_future;
   Complex m_offset;
};
