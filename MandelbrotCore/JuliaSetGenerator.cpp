#include "stdafx.h"
#include "JuliaSetGenerator.h"
#include "ComplexMath.h"

JuliaSetGenerator::JuliaSetGenerator(QObject* parent)
   : FractalSetGenerator(parent)
   , m_iterations(50)
   , m_startPos({0,0})
{}

void JuliaSetGenerator::generateSet(const PlaneMapper& mapper)
{
   if (m_future.isRunning()) return;

   m_future = QtConcurrent::run(std::bind(&JuliaSetGenerator::generateSetInThread, this, mapper));
}

void JuliaSetGenerator::setOffsetPosition(const Complex& offset)
{
   m_offset = offset;
}

void JuliaSetGenerator::generateSetInThread(const PlaneMapper& mapper)
{
   QImage* image = new QImage(mapper.getPlaneDefinition().ViewSize.toSize(), QImage::Format_RGB888);
   //emit imageReady(image);
   //return;
   auto& colorMapping = getColorMapping();
   auto width = image->width();
   auto height = image->height();

   double min = std::numeric_limits<double>::max();
   double max = std::numeric_limits<double>::min();

   long long imageSize = (long long) width * (long long) height;
   double* values = new double[imageSize];

   // getting iterations
#pragma omp parallel for
   for (long long i = 0; i < imageSize; i++)
   {
      auto x = i % width;
      auto y = i / width;

      auto cmp = mapper.translateToComplex({(qreal) x, (qreal) y});
      values[i] = juliaIterations(cmp);
   }

   // minmax
#pragma omp parallel for
   for (long long i = 0; i < imageSize; i++)
   {
      if (values[i] < min)       min = values[i];
      else if (values[i] > max)  max = values[i];
   }

   max = std::max(max, 1.0 / std::numeric_limits<double>::max());

   // writing mapped pixels
#pragma omp parallel for
   for (long long i = 0; i < imageSize; i++)
   {
      auto x = i % width;
      auto y = i / width;

      if (max - min > 0)
         image->setPixelColor(x, y, colorMapping((values[i] - min) / (max - min)));
      else
         image->setPixelColor(x, y, colorMapping(1));
   }

   delete[] values;
   emit imageReady(image);
}

double JuliaSetGenerator::juliaIterations(const Complex& point)
{
   auto current = point;
   double iteration = 0;
   double smooth = std::exp(-magnitude(current));

   while (iteration++ < m_iterations)
   {
      if (magnitudeSquared(current) > 4)
         break;

      current = current * current + m_offset;
      smooth += std::exp(-magnitude(current));
   }

   return smooth;
}
