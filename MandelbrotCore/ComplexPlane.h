#pragma once

#include "stdafx.h"
#include "PlaneDefinition.h"
#include "ColorMapping.h"

class MANDELBROTCORE_EXPORT ComplexPlane
{
public /*constructors*/:

   ComplexPlane();
   ComplexPlane(const PlaneDefinition& def);
   ~ComplexPlane() = default;

public /*functions*/:

   void setSize(const QSizeF& size);
   const QSizeF& getSize() const;

   void setPlaneDefinition(const PlaneDefinition& def);
   const PlaneDefinition& getPlaneDefinition() const;

   void setBuffer(const std::vector<unsigned>& buffer);
   void setBuffer(std::vector<unsigned>&& buffer);
   const std::vector<unsigned>& getBuffer() const;

   QImage toImage(const ColorMapping& mapping) const;

private /*members*/:

   PlaneDefinition m_definition;
   std::vector<unsigned> m_buffer;
   QSizeF m_bufferSize;
};
