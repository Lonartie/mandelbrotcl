#pragma once

#include "stdafx.h"
#include "FractalSetGenerator.h"

class MANDELBROTCORE_EXPORT MandelbrotSetGenerator : public FractalSetGenerator
{
   Q_OBJECT

public /*constructors*/:

    MandelbrotSetGenerator(QObject *parent = Q_NULLPTR);
    virtual ~MandelbrotSetGenerator() = default;
    
public slots:

    virtual void generateSet(const PlaneMapper& mapper) override;

private /*functions*/:
   
   double mandelbrotIterations(const Complex& point);
   void generateSetInThread(const PlaneMapper& mapper);

private /*members*/:

   unsigned m_iterations;
   Complex m_startPos;
   QFuture<void> m_future;

};
