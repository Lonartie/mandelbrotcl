#pragma once

#include "stdafx.h"

class Precise
{
public /*constructors*/:

   Precise();

   Precise(const Precise& o);

   template<typename T>
   Precise(T val);

   Precise(bool custom, std::size_t precision);

   virtual ~Precise() = default;

public /*methods*/:

   void setPrecision(std::size_t precision);
   std::size_t getPrecision() const;

   template<typename T>
   bool compare(std::size_t precision, T val) const;
   bool compare(std::size_t precision, const Precise& val) const;

   std::string to_string() const;

   std::size_t getByteSize() const;

public /*basic operators*/:

   template<typename T>
   Precise& operator=(T val);
   Precise& operator=(const Precise& o);

   template<typename T>
   operator T() const;
   operator Precise() const;

public /*logic operators*/:

   template<typename T>
   bool operator==(T val) const;
   bool operator==(const Precise& o) const;

   template<typename T>
   bool operator>(T val) const;
   bool operator>(const Precise& o) const;

   template<typename T>
   bool operator<(T val) const;
   bool operator<(const Precise& o) const;

   template<typename T>
   bool operator>=(T val) const;
   bool operator>=(const Precise& o) const;

   template<typename T>
   bool operator<=(T val) const;
   bool operator<=(const Precise& o) const;

public /*arithmetic operators*/:

   template<typename T>
   Precise operator+(T val) const;
   Precise operator+(const Precise& o) const;

   template<typename T>
   Precise& operator+=(T val);
   Precise& operator+=(const Precise& o);

   template<typename T>
   Precise operator-(T val) const;
   Precise operator-(const Precise& o) const;

   template<typename T>
   Precise& operator-=(T val);
   Precise& operator-=(const Precise& o);

   template<typename T>
   Precise operator*(T val) const;
   Precise operator*(const Precise& o) const;

   template<typename T>
   Precise& operator*=(T val);
   Precise& operator*=(const Precise& o);

   template<typename T>
   Precise operator/(T val) const;
   Precise operator/(const Precise& o) const;

   template<typename T>
   Precise& operator/=(T val);
   Precise& operator/=(const Precise& o);


private /*methods*/:

   template<typename T>
   void setUpper(T val);

   template<typename T>
   void setLower(T val);

private /*static methods*/:

   static std::vector<unsigned char> createPrecision(std::size_t precision);

private /*members*/:

   bool m_isNegative;
   std::size_t m_upper;
   std::size_t m_precision;
   std::vector<unsigned char> m_lower;
};



/************************************************************************/
/*                              Definition                              */
/************************************************************************/




Precise::Precise()
   : m_isNegative(false)
   , m_upper(0)
   , m_precision(15) // std precision is 15 digits just like double
   , m_lower(createPrecision(m_precision))
{}

Precise::Precise(const Precise& o)
   : m_isNegative(o.m_isNegative)
   , m_upper(o.m_upper)
   , m_precision(o.m_precision)
   , m_lower(o.m_lower)
{}

template<typename T>
Precise::Precise(T val)
   : m_isNegative(false)
   , m_upper(0)
   , m_precision(std::is_floating_point<T>::value ? 15 : 0) // if T is no floating point number, we need no precision!
   , m_lower(createPrecision(m_precision))
{
   operator=<T>(val);
}

Precise::Precise(bool custom, std::size_t precision)
   : m_isNegative(false)
   , m_upper(0)
   , m_precision(precision) // if T is no floating point number, we need no precision!
   , m_lower(createPrecision(m_precision))
{
   m_lower[m_precision - 1] = 1;
}


void Precise::setPrecision(std::size_t precision)
{
   m_precision = precision;
   auto precise = createPrecision(precision);
   auto min = std::min(m_lower.size(), precise.size());

   for (std::size_t level = 0; level < min; level++)
      precise[level] = m_lower[level];

   m_lower = precise;
}

std::size_t Precise::getPrecision() const
{
   return m_precision;
}

template<typename T>
bool Precise::compare(std::size_t precision, T val) const
{
   return compare(precision, Precise(val));
}

bool Precise::compare(std::size_t precision, const Precise& val) const
{
   auto dif = std::abs<Precise>(std::abs<Precise>(*this) - std::abs<Precise>(val));
   Precise max(0);
   max.setPrecision(precision);
   max.m_lower[precision - 1] = 1;
   return dif < max;
}


std::string Precise::to_string() const
{
   std::string str = (m_isNegative ? "-" : "");
   str += std::to_string(m_upper);

   if (m_precision > 0)
      str += ".";

   for (std::size_t level = 0; level < m_precision; level++)
      str += std::to_string(m_lower[level]);

   return str;
}

std::size_t Precise::getByteSize() const
{
   /*   bool m_isNegative;
   std::size_t m_upper;
   std::size_t m_precision;
   std::vector<unsigned char> m_lower;
   */

   return sizeof(bool) /*m_isNegative*/
      + sizeof(std::size_t) /*m_upper*/
      + sizeof(std::size_t) /*m_precision*/
      + sizeof(unsigned char) * m_precision /*m_lower*/;
}

template<typename T>
Precise& Precise::operator=(T val)
{
   setUpper(val);
   setLower(val);

   return *this;
}

Precise& Precise::operator=(const Precise& o)
{
   m_isNegative = o.m_isNegative;
   m_upper = o.m_upper;
   m_precision = o.m_precision;
   m_lower = o.m_lower;

   return *this;
}

template<typename T>
Precise::operator T() const
{
   T value = m_upper;

   if (std::is_floating_point<T>::value)
      for (std::size_t level = 0; level < m_precision; level++)
         value += m_lower[level] / std::pow(10, level + 1);

   if (m_isNegative) 
      value = value * (-1);

   return value;
}

Precise::operator Precise() const
{
   return Precise(*this);
}

template<typename T>
bool Precise::operator==(T val) const
{
   return (*this) == Precise(val);
}

bool Precise::operator==(const Precise& o) const
{
   // upper part
   if (m_isNegative != o.m_isNegative || m_upper != o.m_upper) return false;

   // rough lower part
   if (m_precision == o.m_precision && m_lower == o.m_lower) return true;

   auto min = std::min(m_precision, o.m_precision);

   // detailed lower part same length
   for (std::size_t level = 0; level < min; level++)
      if (m_lower[level] != o.m_lower[level]) return false;

   // we are more precise
   for (std::size_t level = min; level < m_precision; level++)
      if (m_lower[level] != 0) return false;

   // other is more precise
   for (std::size_t level = min; level < o.m_precision; level++)
      if (o.m_lower[level] != 0) return false;

   // everything ok
   // there are differences but they sum up to 0 so it doesn't matter
   return true;
}


template<typename T>
bool Precise::operator>=(T val) const
{
   return operator>(val) || operator==(val);
}

bool Precise::operator>=(const Precise& o) const
{
   return operator>(o) || operator==(o);
}

template<typename T>
bool Precise::operator>(T val) const
{
   // negativity
   if (val < 0 != m_isNegative) return !m_isNegative;

   // upper 
   val = std::abs(val);
   if (static_cast<std::size_t>(val) != m_upper) return m_upper > static_cast<std::size_t>(val);

   // lower
   for (std::size_t level = 0; level < m_precision; level++)
   {
      val = val - static_cast<std::size_t>(val);
      val *= 10;
      if (m_lower[level] != static_cast<std::size_t>(val))
         return m_lower[level] > static_cast<std::size_t>(val);
   }

   // overhang
   return !(val >= 0);
}

bool Precise::operator>(const Precise& o) const
{
   // negativity
   if (o.m_isNegative != m_isNegative) return !m_isNegative;

   // upper 
   if (o.m_upper != m_upper) return m_upper > o.m_upper;

   // lower
   auto min = std::min(m_precision, o.m_precision);
   for (std::size_t level = 0; level < min; level++)
      if (m_lower[level] != o.m_lower[level]) return m_lower[level] > o.m_lower[level];

   // we are more precise
   for (std::size_t level = min; level < m_precision; level++)
      if (m_lower[level] != 0) return true;

   // other is more precise
   for (std::size_t level = min; level < o.m_precision; level++)
      if (o.m_lower[level] != 0) return false;

   // equality
   return false;
}

template<typename T>
bool Precise::operator<=(T val) const
{
   return operator<(val) || operator==(val);
}

bool Precise::operator<=(const Precise& o) const
{
   return operator<(o) || operator==(o);
}

template<typename T>
bool Precise::operator<(T val) const
{
   // negativity
   if (val < 0 != m_isNegative) return m_isNegative;

   // upper 
   val = std::abs(val);
   if (static_cast<std::size_t>(val) != m_upper) return m_upper < static_cast<std::size_t>(val);

   // lower
   for (std::size_t level = 0; level < m_precision; level++)
   {
      val = val - static_cast<std::size_t>(val);
      val *= 10;
      if (m_lower[level] != static_cast<std::size_t>(val))
         return m_lower[level] < static_cast<std::size_t>(val);
   }

   // overhang
   return (val >= 0);
}

bool Precise::operator<(const Precise& o) const
{
   // negativity
   if (o.m_isNegative != m_isNegative) return m_isNegative;

   // upper 
   if (o.m_upper != m_upper) return m_upper < o.m_upper;

   // lower
   auto min = std::min(m_precision, o.m_precision);
   for (std::size_t level = 0; level < min; level++)
      if (m_lower[level] != o.m_lower[level]) return m_lower[level] < o.m_lower[level];

   // we are more precise
   for (std::size_t level = min; level < m_precision; level++)
      if (m_lower[level] != 0) return false;

   // other is more precise
   for (std::size_t level = min; level < o.m_precision; level++)
      if (o.m_lower[level] != 0) return true;

   // equality
   return false;
}

template<typename T>
Precise Precise::operator+(T val) const
{
   return (*this) + Precise(val);
}

Precise Precise::operator+(const Precise& o) const
{
   if (o.m_isNegative) return operator-(o * -1);
   if (m_isNegative) return o - ((*this) * -1);

   Precise other(o);
   Precise cpy(*this);

   if (cpy.m_precision < other.m_precision)
      cpy.setPrecision(other.m_precision);
   else if (other.m_precision < cpy.m_precision)
      other.setPrecision(cpy.m_precision);

   // lower first (back to front because transfers are easier that way)
   if (cpy.m_precision > 0)
   {
      for (std::size_t level = cpy.m_precision - 1; level > 0; level--)
      {
         cpy.m_lower[level] += other.m_lower[level];
         while (cpy.m_lower[level] > 9)
         {
            cpy.m_lower[level] -= 10;
            cpy.m_lower[level - 1] += 1;
         }
      }
      cpy.m_lower[0] += other.m_lower[0];
      while (cpy.m_lower[0] > 9)
      {
         cpy.m_lower[0] -= 10;
         cpy.m_upper += 1;
      }
   }

   // upper
   cpy.m_upper += other.m_upper;

   return cpy;
}

template<typename T>
Precise& Precise::operator+=(T val)
{
   return (*this) += Precise(val);
}

Precise& Precise::operator+=(const Precise& o)
{
   if (o.m_isNegative) return operator-= (o * -1); // a + (-b) = a - b
   if (m_isNegative) return (*this) = (o - (*this * -1)); // -a + b = b - a

   Precise other(o);

   if (m_precision < other.m_precision)
      setPrecision(other.m_precision);
   else if (other.m_precision < m_precision)
      other.setPrecision(m_precision);

   // lower first (back to front because transfers are easier that way)
   if (m_precision > 0)
   {
      for (std::size_t level = m_precision - 1; level > 0; level--)
      {
         m_lower[level] += other.m_lower[level];
         while (m_lower[level] > 9)
         {
            m_lower[level] -= 10;
            m_lower[level - 1] += 1;
         }
      }
      m_lower[0] += other.m_lower[0];
      while (m_lower[0] > 9)
      {
         m_lower[0] -= 10;
         m_upper += 1;
      }
   }

   // upper
   m_upper += other.m_upper;

   return *this;
}

template<typename T>
Precise Precise::operator-(T val) const
{
   return (*this) - Precise(val);
}

Precise Precise::operator-(const Precise& o) const
{
   if (o.m_isNegative) return (*this) + (o * -1); // a - (-b) = a + b
   if (m_isNegative) return ((*this * -1) + o) * -1; // -a - b = -(a + b)
   if ((*this) < o) return (o - (*this)) * -1; // a - b = -(b - a) --> wenn a < b

   Precise other(o);
   Precise cpy(*this);

   if (cpy.m_precision < other.m_precision)
      cpy.setPrecision(other.m_precision);
   else if (other.m_precision < cpy.m_precision)
      other.setPrecision(cpy.m_precision);

   // lower first (back to front because transfers are easier that way)
   if (cpy.m_precision > 0)
   {
      for (std::size_t level = cpy.m_precision - 1; level > 0; level--)
      {
         // transfer
         if (cpy.m_lower[level] < other.m_lower[level])
         {
            cpy.m_lower[level] += 10;
            other.m_lower[level - 1]++;
         }

         cpy.m_lower[level] -= other.m_lower[level];
      }

      if (cpy.m_lower[0] < other.m_lower[0])
      {
         cpy.m_lower[0] += 10;
         other.m_upper++;
      }

      cpy.m_lower[0] -= other.m_lower[0];
   }

   // upper
   cpy.m_upper -= other.m_upper;

   return cpy;
}

template<typename T>
Precise& Precise::operator-=(T val)
{
   return (*this) -= Precise(val);
}

Precise& Precise::operator-=(const Precise& o)
{
   if (o.m_isNegative) return (*this) += (o * -1); // a - (-b) = a + b
   if (m_isNegative) return (*this) = ((*this * -1) + o) * -1; // -a - b = -(a + b)
   if ((*this) < o) return (*this) = (o - (*this)) * -1; // a - b = -(b - a) --> wenn a < b

   Precise other(o);

   if (m_precision < other.m_precision)
      setPrecision(other.m_precision);
   else if (other.m_precision < m_precision)
      other.setPrecision(m_precision);

   // lower first (back to front because transfers are easier that way)
   if (m_precision > 0)
   {
      for (std::size_t level = m_precision - 1; level > 0; level--)
      {
         // transfer
         if (m_lower[level] < other.m_lower[level])
         {
            m_lower[level] += 10;
            other.m_lower[level - 1]++;
         }

         m_lower[level] -= other.m_lower[level];
      }

      if (m_lower[0] < other.m_lower[0])
      {
         m_lower[0] += 10;
         other.m_upper++;
      }

      m_lower[o] -= other.m_lower[0];
   }

   // upper
   m_upper -= other.m_upper;

   return *this;
}

template<typename T>
Precise Precise::operator*(T val) const
{
   return (*this) * Precise(val);
}

Precise Precise::operator*(const Precise& o) const
{
   Precise cpy(*this);
   Precise other(o);
   if (other < 0)
   {
      cpy.m_isNegative = !cpy.m_isNegative;
      other.m_isNegative = !other.m_isNegative;
   }

   return cpy;
}

template<typename T>
Precise& Precise::operator*=(T val)
{
   return (*this) *= Precise(val);
}

Precise& Precise::operator*=(const Precise& o)
{
   Precise other(o);
   if (other < 0)
   {
      m_isNegative = !m_isNegative;
      other.m_isNegative = !other.m_isNegative;
   }

   return *this;
}

template<typename T>
Precise Precise::operator/(T val) const
{
   return (*this) / Precise(val);
}

Precise Precise::operator/(const Precise& o) const
{
   // TODO implement!
   return *this;
}

template<typename T>
Precise& Precise::operator/=(T val)
{
   return (*this) /= Precise(val);
}

Precise& Precise::operator/=(const Precise& o)
{
   // TODO implement!
   return *this;
}

template<typename T>
void Precise::setUpper(T val)
{
   m_isNegative = val < 0;
   m_upper = static_cast<std::size_t>(std::abs<T>(val));
}

template<typename T>
void Precise::setLower(T val)
{
   val = std::abs<T>(val);
   val = val - static_cast<std::size_t>(val);

   for (std::size_t level = 0; level < m_precision; level++)
   {
      val *= 10;
      m_lower[level] = val;
      val = val - static_cast<std::size_t>(val);
   }
}

std::vector<unsigned char> Precise::createPrecision(std::size_t precision)
{
   std::vector<unsigned char> precise;
   precise.resize(precision);
   std::fill(precise.begin(), precise.end(), 0);
   return precise;
}

/************************************************************************/
/*                          external operators                          */
/************************************************************************/

namespace std
{
   Precise abs(const Precise& o)
   {
      return (o < 0 ? o * -1 : o);
   }

   ostream& operator<<(ostream& stream, const Precise& pr)
   {
      return stream << pr.to_string();
   }
}

#pragma warning(push)
#pragma warning(disable : 4455)

Precise operator ""p(std::size_t val)
{
   return Precise(val);
}

Precise operator ""p(long double val)
{
   return Precise(val);
}

#pragma warning(pop)

/************************************************************************/
/*                          pre-defined types                           */
/************************************************************************/

static const Precise __milli  (true,  3);
static const Precise __micro  (true,  6);
static const Precise __nano   (true,  9);
static const Precise __pico   (true, 12);
static const Precise __femto  (true, 15);
static const Precise __atto   (true, 18);
static const Precise __zepto  (true, 21);
static const Precise __yocto  (true, 24);