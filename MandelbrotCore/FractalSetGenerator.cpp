#include "stdafx.h"

#include "FractalSetGenerator.h"

FractalSetGenerator::FractalSetGenerator(QObject* parent /*= Q_NULLPTR*/)
   : QObject(parent)
{}

void FractalSetGenerator::setColorMapper(const ColorMapping & mapping)
{
   m_colorMapping = mapping;
}

const ColorMapping& FractalSetGenerator::getColorMapping() const
{
   return m_colorMapping;
}
