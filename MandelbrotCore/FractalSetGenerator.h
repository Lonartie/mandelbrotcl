#pragma once

#include "stdafx.h"
#include "PlaneMapper.h"

class MANDELBROTCORE_EXPORT FractalSetGenerator: public QObject
{
   Q_OBJECT

public:
   
   FractalSetGenerator(QObject* parent = Q_NULLPTR);
   virtual ~FractalSetGenerator() = default;

signals:

   void imageReady(QImage* image);

public slots:
   
   virtual void generateSet(const PlaneMapper& mapper) = 0;
   void setColorMapper(const ColorMapping& mapping);

protected /*functions*/:

   const ColorMapping& getColorMapping() const;

private /*members*/:

   ColorMapping m_colorMapping;
};