#pragma once

#include "stdafx.h"
#include "PlaneDefinition.h"
#include "ComplexPlane.h"

class MANDELBROTCORE_EXPORT PlaneMapper
{
public /*constructors*/:

   enum Axis { X, Y };

   PlaneMapper();
   PlaneMapper(const PlaneMapper& o);
   PlaneMapper(const PlaneDefinition& plane);
   PlaneMapper(const ComplexPlane& plane);
   ~PlaneMapper() = default;

public /*functions*/:

   QPointF translateToPixel(const Complex& point) const;
   Complex translateToComplex(const QPointF& point) const;

   long double translateToPixel(long double point, Axis axis) const;
   long double translateToComplex(long double point, Axis axis) const;

   std::array<Complex, 4> getBounds(const QSizeF& viewSize) const;

   long double pixelPerComplex(Axis axis) const;
   long double complexPerPixel(Axis axis) const;

   const PlaneDefinition& getPlaneDefinition() const;
   PlaneDefinition& getPlaneDefinition();
   void setPlaneDefinition(const PlaneDefinition& plane);

private /*members*/:

   PlaneDefinition m_plane;

};
