#include "stdafx.h"
#include "ColorMapping.h"
#include "ComplexMath.h"

std::vector<ColorMapping> ColorMapping::registered;

bool ColorMapping::registerMapping(const ColorMapping& mapping)
{
   registered.push_back(mapping);
   return true;
}

ColorMapping::ColorMapping(ChannelFunction r, ChannelFunction g, ChannelFunction b)
   : m_rfunc(r)
   , m_gfunc(g)
   , m_bfunc(b)
{}

ColorMapping::ColorMapping()
{
   m_rfunc = m_gfunc = m_bfunc = [](auto value) {return 255 - value * 255; };
}

QImage ColorMapping::toImage() const
{
   QImage preview(10, 100, QImage::Format_RGB888);

   for (int n = 0; n < 100; n++)
   {
      auto col = operator()(n / 100.0);
      for (int i = 0; i < 10; i++)
      {
         preview.setPixelColor(i, n, col);
      }
   }

   return preview;
}

QColor ColorMapping::operator()(long double value) const
{
   value = clamp(0, 1, value);
   if (m_rfunc && m_gfunc && m_bfunc)
   return QColor::fromRgb(m_rfunc(value), m_gfunc(value), m_bfunc(value));
   return Qt::white;
}
