#include "stdafx.h"
#include "ComplexMath.h"

Complex roundToDecimalPrecision(long double precision, const Complex& c)
{
   return Complex
   {
      std::round(c.real() * std::pow(10, precision)) / std::pow(10, precision),
      std::round(c.imag() * std::pow(10, precision)) / std::pow(10, precision)
   };
}

Complex roundToDecimalPrecision(long double precision_real, long double precision_imag, const Complex& c)
{
   auto real = c.real();
   auto pow = std::pow(10, precision_real);
   auto inside = real * pow;
   auto rounded = std::round(inside);
   auto result = rounded / pow;
   return Complex
   {
      result,
      std::round(c.imag() * std::pow(10, precision_imag)) / std::pow(10, precision_imag)
   };
}

QPointF roundToDecimalPrecision(long double precision, const QPointF& c)
{
   return QPointF
   {
      (qreal) (std::round(c.x() * std::pow(10, precision)) / std::pow(10, precision)),
      (qreal) (std::round(c.y() * std::pow(10, precision)) / std::pow(10, precision))
   };
}

double roundToDecimalPrecision(long double precision, double c)
{
   return std::round(c * std::pow(10, precision)) / std::pow(10, precision);
}

const long double tweak = -std::log10(0.5);

long double nearest10nthBase(long double num)
{
   return std::pow(10L, std::floor(std::log10(num) + tweak));
}

long double MANDELBROTCORE_EXPORT magnitudeSquared(const Complex& point)
{
   return point.real() * point.real() + point.imag() * point.imag();
}

long double MANDELBROTCORE_EXPORT magnitude(const Complex& point)
{
   return std::sqrt(magnitudeSquared(point));
}

long double MANDELBROTCORE_EXPORT clamp(long double min, long double max, long double value)
{
   return (value < min ? min : value > max ? max : value);
}

