#include "stdafx.h"
#include "ComplexPlane.h"

ComplexPlane::ComplexPlane() 
   : m_definition() { }

ComplexPlane::ComplexPlane(const PlaneDefinition& def)
   : m_definition(def) { }

void ComplexPlane::setSize(const QSizeF& size)
{
   m_bufferSize = size;
   m_buffer.resize((std::size_t)size.width() * (std::size_t)size.height(), 0);
}

const QSizeF& ComplexPlane::getSize() const
{
   return m_bufferSize;
}

void ComplexPlane::setPlaneDefinition(const PlaneDefinition& def)
{
   m_definition = def;
}

const PlaneDefinition& ComplexPlane::getPlaneDefinition() const
{
   return m_definition;
}

void ComplexPlane::setBuffer(const std::vector<unsigned>& buffer)
{
   m_buffer = buffer;
}

void ComplexPlane::setBuffer(std::vector<unsigned>&& buffer)
{
   m_buffer = std::move(buffer);
}

const std::vector<unsigned>& ComplexPlane::getBuffer() const
{
   return m_buffer;
}

QImage ComplexPlane::toImage(const ColorMapping& mapping) const
{
   // TODO Fill Buffer here!
   return QImage();
}
