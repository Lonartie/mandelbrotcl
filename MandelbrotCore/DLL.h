#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(MANDELBROTCORE_LIB)
#  define MANDELBROTCORE_EXPORT Q_DECL_EXPORT
# else
#  define MANDELBROTCORE_EXPORT Q_DECL_IMPORT
# endif
#else
# define MANDELBROTCORE_EXPORT
#endif
