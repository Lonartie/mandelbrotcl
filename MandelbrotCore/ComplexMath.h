#pragma once

#include "stdafx.h"

Complex MANDELBROTCORE_EXPORT roundToDecimalPrecision(long double precision, const Complex& c);
Complex MANDELBROTCORE_EXPORT roundToDecimalPrecision(long double precision_real, long double precision_imag, const Complex& c);
QPointF MANDELBROTCORE_EXPORT roundToDecimalPrecision(long double precision, const QPointF& c);
double MANDELBROTCORE_EXPORT roundToDecimalPrecision(long double precision, double c);

long double MANDELBROTCORE_EXPORT nearest10nthBase(long double num);

long double MANDELBROTCORE_EXPORT magnitudeSquared(const Complex& point);
long double MANDELBROTCORE_EXPORT magnitude(const Complex& point); 

long double MANDELBROTCORE_EXPORT clamp(long double min, long double max, long double value);