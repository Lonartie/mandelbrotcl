#include "stdafx.h"
#include "PlaneMapper.h"

PlaneMapper::PlaneMapper()
   : m_plane() {}

PlaneMapper::PlaneMapper(const PlaneMapper& o)
   : m_plane(o.m_plane) {}

PlaneMapper::PlaneMapper(const PlaneDefinition& plane)
   : m_plane(plane) {}

PlaneMapper::PlaneMapper(const ComplexPlane& plane)
   : m_plane(plane.getPlaneDefinition()) {}

QPointF PlaneMapper::translateToPixel(const Complex& point) const
{
   return QPointF
   {
      (qreal) translateToPixel(point.real(), X),
      (qreal) translateToPixel(point.imag(), Y)
   };
}

Complex PlaneMapper::translateToComplex(const QPointF& point) const
{
   return Complex
   {
      translateToComplex(point.x(), X),
      translateToComplex(point.y(), Y)
   };
}

long double PlaneMapper::translateToPixel(long double point, Axis axis) const
{
   double factor, offset, origin;

   if (axis == X)
   {
      factor = m_plane.ScaleX * m_plane.GlobalZoom;
      offset = m_plane.GlobalOffset.real();
      origin = m_plane.ViewSize.width() / 2;
   } else
   {
      factor = m_plane.ScaleY * m_plane.GlobalZoom;
      offset = m_plane.GlobalOffset.imag();
      origin = m_plane.ViewSize.height() / 2;
   }

   return (point + offset) * factor + origin;
}

long double PlaneMapper::translateToComplex(long double point, Axis axis) const
{
   double factor, offset, origin;

   if (axis == X)
   {
      factor = m_plane.ScaleX * m_plane.GlobalZoom;
      offset = m_plane.GlobalOffset.real();
      origin = m_plane.ViewSize.width() / 2;
   } else
   {
      factor = m_plane.ScaleY * m_plane.GlobalZoom;
      offset = m_plane.GlobalOffset.imag();
      origin = m_plane.ViewSize.height() / 2;
   }

   return (point - origin) / factor - offset;
}

std::array<Complex, 4> PlaneMapper::getBounds(const QSizeF& viewSize) const
{
   return
   {
      translateToComplex({0,0}),
      translateToComplex({viewSize.width(), 0}),
      translateToComplex({0, viewSize.height()}),
      translateToComplex({viewSize.width(),viewSize.height()}),
   };
}

long double PlaneMapper::pixelPerComplex(Axis axis) const
{
   return (m_plane.GlobalZoom * (axis == X ? m_plane.ScaleX : m_plane.ScaleY));
}

long double PlaneMapper::complexPerPixel(Axis axis) const
{
   return 1 / (m_plane.GlobalZoom * (axis == X ? m_plane.ScaleX : m_plane.ScaleY));
}

const PlaneDefinition& PlaneMapper::getPlaneDefinition() const
{
   return m_plane;
}

PlaneDefinition& PlaneMapper::getPlaneDefinition()
{
   return m_plane;
}

void PlaneMapper::setPlaneDefinition(const PlaneDefinition& plane)
{
   m_plane = plane;
}
