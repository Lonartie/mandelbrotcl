#pragma once

#include "stdafx.h"

class MANDELBROTCORE_EXPORT ColorMapping 
{
public /*registry*/:

   static std::vector<ColorMapping> registered;
   static bool registerMapping(const ColorMapping& mapping);

public /*constructor*/:

   using ChannelFunction = std::function<int(long double)>;

   ColorMapping();
   ColorMapping(ChannelFunction r, ChannelFunction g, ChannelFunction b);

public /*functions*/:

   QImage toImage() const;

public /*operators*/:

   QColor operator()(long double value) const;

private /*members*/:

   ChannelFunction m_rfunc, m_gfunc, m_bfunc;

};
