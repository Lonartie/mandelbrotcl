#pragma once

#include "stdafx.h"

struct PlaneDefinition
{
   QSizeF ViewSize = {0,0};
   Complex GlobalOffset = {0,0};
   double GlobalZoom = 100.0;
   double ScaleX = 1.0;
   double ScaleY = -1.0;
};
